import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'dart:io';
import 'dart:convert';
import 'package:hackaton/app_config.dart' as env;
import 'package:hackaton/services/tts-service.dart';
import 'package:hackaton/ui/pages/settings-page.dart';
import 'package:hackaton/services/server-requests.dart';
import 'package:hackaton/langs.dart';
import 'dart:io';
import 'package:hackaton/services/play-sound.dart';
import 'package:hackaton/core/jsonHelper.dart';

class PromptsTable extends StatelessWidget {
  PromptsTable({super.key, required this.imagePath});
  final String imagePath;

    List<String> prompt_cache = [];
    List<String> agent_response_cache = [];
    @override
        Widget build(BuildContext context) {
            // this.image = File(imagePath);
            return FutureBuilder(
                    future: promptsFile,
                    builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                    // print(jsonDecode(snapshot.data!.readAsStringSync()));
                    var json = jsonDecode(snapshot.data!.readAsStringSync());
                    var prompts = json['prompts'];
                    var max_len = json['maxTokenLength'] ?? 100;
                    var lang_in = json["inputLanguage"];
                    var lang_out = json["outputLanguage"];
                    var prompt_len = prompts.length;
                    List<Widget> gridChildren = List.generate(prompt_len, (int index) {
                            int row = index ~/ 3; // integer division to get current row
                            int col = index % 3;  // modulo operator to get current column
                            Color color = env.config_background_color;

                            return  Column(
                                    children: [
                                    Container(
                                        height: 80,
                                        width: MediaQuery.of(context).size.width,
                                        child: ElevatedButton(
                                            onPressed: () async {
                                              if (locked) {
                                                EasyLoading.showError("Other prompt is being processed. Please wait.");
                                                return;
                                              }
                                                File imageFile = File(this.imagePath);
                                                prompt_cache.add(prompts[index]);
                                                var response = await sendPictureAndPrompt(
                                                    prompts[index], 
                                                    imageFile,
                                                    lang_in,
                                                    lang_out,
                                                    max_len ,
                                                    prompt_cache,
                                                    agent_response_cache
                                                    );
                                                agent_response_cache.add(response);


                    },
                    style: FilledButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      // backgroundColor: env.config_accent_color,
                      // foregroundColor: env.config_foreground_color,
                    ),
                    child: Text(
                      style: const TextStyle(color: env.config_foreground_color, fontSize: 20),
                      "${prompts[index]}",
                    ),
                  ),
                ),
              ],
            );
          });
          return ListView.builder(
            itemCount: gridChildren.length,
            itemBuilder: (context, index) {
              final tileModel = gridChildren[index];
              return Container(
                decoration: BoxDecoration(border: Border.all()),
                padding: const EdgeInsets.all(8),
                margin: const EdgeInsets.symmetric(vertical: 1),
                height: 100,
                alignment: Alignment.centerLeft,
                child: tileModel,
              );
            },
          );
        }
        return const CircularProgressIndicator();
      },
    );
  }
}
