import 'package:flutter/material.dart';
import 'package:hackaton/app_config.dart' as env;

class TopBar extends StatelessWidget {
  TopBar({super.key});
    Widget title = const Text("Now you see me");
    var backgroundColor = Colors.black54;
    var foregroundColor = Colors.white;

    @override
    Widget build(BuildContext context) {
        return AppBar(
            title: title,
            actions: <Widget>[Ink(decoration: BoxDecoration(shape: BoxShape.circle, color: env.config_secondary_background_color), child:IconButton(
                            icon: Icon(Icons.settings, color: Colors.white),
                            // backgroundColor: Colors.white,
                            onPressed: ()=> (on_settings_button_pressed(context)),
                            ))],
            backgroundColor: backgroundColor,
            foregroundColor: foregroundColor,
        );
    }

    void on_settings_button_pressed(BuildContext context) {
        print("settings pressed");
        Navigator.pushNamed(context, '/settings');
    }


}
