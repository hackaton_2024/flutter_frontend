import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:hackaton/core/camera-picture-screen.dart';
import 'package:camera/camera.dart';
import 'package:hackaton/core/jsonHelper.dart';
import 'package:hackaton/services/tts-service.dart';

class HomePage extends StatelessWidget {
  HomePage({super.key, required this.camera});
  final CameraDescription camera;
  final TTSService ttsService = TTSService();
  bool dragging = false;

  @override
  Widget build(BuildContext context) {
    var camScreen_key = GlobalKey<CameraPictureScreenState>();

    camScreen_key.currentState?.initState();

    var camScreen = CameraPictureScreen(
      key: camScreen_key,
      camera: camera,
    );

    void takePhoto() async {
      camScreen_key.currentState?.on_take_photo();
    }

    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          GestureDetector(
            onLongPress: () {
              if (!dragging) {
                camScreen_key.currentState?.on_flashlight(false);
              }
            },
            onVerticalDragStart: (DragStartDetails details) {
              dragging = true;
            },
            onVerticalDragUpdate: (DragUpdateDetails details) async {
              camScreen_key.currentState?.on_zoom(details.delta.dy!);
            },
            onVerticalDragEnd: (DragEndDetails details) {
              dragging = false;
            },

            onHorizontalDragEnd: (DragEndDetails details) {
              camScreen_key.currentState?.on_focus();
              dragging = false;
            },
            child: camScreen,
          ),
          Expanded(
            child: Center(
              child: Semantics(
                label: 'Tlačítko na focení',
                button: true,
                child: Container(
                  padding: const EdgeInsets.all(20),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: FilledButton(
                      onPressed: takePhoto,
                      child: const Stack(
                        children: <Widget>[
                          Text(
                            'Capture',
                            style: TextStyle(color: Colors.transparent),
                            semanticsLabel: 'Tlačítko pro focení',
                          ),
                          Icon(Icons.camera_alt, size: 50),
                        ],
                      ),
                      style: FilledButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        backgroundColor: env.config_accent2_color,
                        foregroundColor: env.config_foreground_color,
                        // padding: EdgeInsets.all(20),
                      )),
                ),
              ),
            ),
          ),
        ]);
  }
}
