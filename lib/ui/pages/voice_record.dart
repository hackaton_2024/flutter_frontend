import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:hackaton/ui/components/prompts-table.dart'; // Ensure this import is correct
import 'package:hackaton/app_config.dart' as env;
import 'package:hackaton/ui/components/top-bar.dart';
import 'package:hackaton/ui/pages/results.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;
import 'dart:io';
import 'package:hackaton/langs.dart';
import 'package:hackaton/services/server-requests.dart';

class VoiceRecordPage extends StatefulWidget {
  const VoiceRecordPage({Key? key, required this.imagePath}) : super(key: key);
  final String imagePath;

  @override
  _VoiceRecordPageState createState() => _VoiceRecordPageState();
}

class _VoiceRecordPageState extends State<VoiceRecordPage> {
  late stt.SpeechToText _speech;
  bool _isListening = false;
  String _lastWords = ""; // Variable to store the last recognized words
  Timer? _holdTimer;
  List<String> promptCache = []; // Cache for prompts
  List<String> responseCache = []; // Cache for responses

  @override
  void initState() {
    super.initState();
    _speech = stt.SpeechToText();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(55),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: TopBar(),
        ),
      ),
      body: Column(
        children: [
          // The prompts table widget
          Expanded(child: PromptsTable(imagePath: widget.imagePath)),
          // The section for the voice recording button
          Container(
            padding: EdgeInsets.symmetric(horizontal: 40, vertical: 40),
            width: double.infinity,
            child: GestureDetector(
              onLongPressStart: (details) => _manageRecording(true),
              onLongPressEnd: (details) => _manageRecording(false),
              child: FilledButton(
                onPressed:
                    () {}, // This function is required but we handle press via GestureDetector
                style: FilledButton.styleFrom(
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  padding: const EdgeInsets.all(25),
                  foregroundColor: env.config_foreground_color,
                  backgroundColor: env.config_accent2_color,

                ),
                child: const Icon(Icons.mic, size: 40),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _manageRecording(bool start) {
    if (start) {
      _startRecording();
      // Check every 300ms if the button is still being pressed.
      _holdTimer = Timer.periodic(Duration(milliseconds: 300), (timer) {
        if (!_isListening) {
          timer.cancel(); // Stop the timer if recording has stopped
        }
      });
    } else {
      // After release, add an additional delay of 3 seconds before stopping the recording.
      Future.delayed(Duration(seconds: 3), () {
        _holdTimer?.cancel(); // Stop the periodic check
        if (_isListening) {
          _stopRecording();
        }
      });
    }
  }

 void _startRecording() async {
  final available = await _speech.initialize(
    onStatus: (val) => print('onStatus: $val'),
    onError: (val) => print('onError: $val')
  );
  if (available) {
    setState(() => _isListening = true);
    // Specify the locale for Czech language during listening
    _speech.listen(
      onResult: (val) => setState(() => _lastWords = val.recognizedWords),
      localeId: 'cs-CZ', // Set Czech as the main language for speech recognition
    );
  } else {
    setState(() => _isListening = false);
    print(_lastWords);
  }
}
  void _stopRecording() async {
    _speech.stop();
    setState(() => _isListening = false);
    File imageFile = File(widget.imagePath);
    print(_lastWords);

    String prompt =
        "Jsem slepý člověk. Tato fotka co jsem ti poskytl, ukazuje můj pohled. Dále se budu ptát a ty mi budeš obrázek popisovat jen na základně mích otázek, nikoliv jinak. Zachovej velmi krátkou odpověd. Moje otázka: $_lastWords";

    // Check if the prompt is already in cache
    int promptIndex = promptCache.indexOf(prompt);
    
      if(promptCache.isEmpty)     promptCache.add(prompt);
  

      // If not cached, send the request

      var response = await sendPictureAndPrompt(
          prompt, imageFile, Lang.cz.lang_str, Lang.cz.lang_str, 100, promptCache, responseCache);

      // Cache the new prompt and its response
      
      //if(responseCache.isEmpty)  responseCache.add(response);
      print("Response: $response");
    
  }

  /*Future<void> _processSpeechToTextResult(String text) async {
    EasyLoading.show(status: "Loading...");
    var response = await http.get(Uri.parse("https://ifconfig.me/ip"));
    var ipString = response.body;
    EasyLoading.dismiss();

    if (mounted) {
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => ResultsPage(
            ip: ipString, imagePath: widget.imagePath, resultText: text),
      ));
    }
  }*/
}
