import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:audioplayers/audioplayers.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

// List<bool> TTSClearHistory = [false];
AudioPlayer audioPlayer = AudioPlayer();
bool TTSLocked = false;

class TTSService {
  // List<bool> localTTSClearHistory = TTSClearHistory; // initialize new history

  Future<void> tts_api(String text) async {
    TTSLocked = true;

    print("TSS");
    var url = Uri.parse('http://147.228.125.108:9999/v4/synth')
        .replace(queryParameters: {
      'text': text,
      'engine': 'Ilona',
      'format': 'wav',
    });

    HapticFeedback.lightImpact();
    EasyLoading.show(status: "Synthesizing...");

    var response = await http.get(url);
    String filePath;

    EasyLoading.dismiss();

    if (response.statusCode == 200) {
      // Get the temporary directory of the device
      var tempDir = await getApplicationSupportDirectory();
      filePath = '${tempDir.path}/tempAudio.wav';

      // Write the file
      var file = File(filePath);
      await file.writeAsBytes(response.bodyBytes);


      // Clear any other audio first
      audioPlayer.stop();

      // Wait for 100 ms
      await Future.delayed(const Duration(milliseconds: 100));

      HapticFeedback.heavyImpact();

      EasyLoading.showSuccess("Playing");
  
      // Set the audio source to the file and play
      await audioPlayer.play(DeviceFileSource(filePath));

      TTSLocked = false;

      // costantly check if ttsClear is true
      // await Isolate.spawn(TTSCheckHistory, ["this must be here"]);

      print("Successfully synthesized and playing from file.");
      
    } else {
      TTSLocked = false;
      print("Failed to save file. Status code: ${response.statusCode}");
      EasyLoading.showError("Failed to save file. Status code: ${response.statusCode}");
    }
  }

  // void TTSCheckHistory(List<String> randomThing) async {
  //   while (true) {
  //     await Future.delayed(const Duration(milliseconds: 100));
  //     if (_audioPlayer.playing && TTSClearHistory != localTTSClearHistory) {
  //       _audioPlayer.stop();
  //       localTTSClearHistory = TTSClearHistory;
  //       print("TTS cleared");
  //       break;
  //     }
  //   }
  // }
}
