import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:hackaton/app_config.dart' as env;
import 'package:hackaton/langs.dart';
import 'dart:convert';
import 'dart:io';
import 'package:hackaton/services/tts-service.dart';


TTSService ttsService = TTSService();
bool locked = false;


Future<String> sendPictureAndPrompt(String prompt,File imageFile, String lang_in, String lang_out, int max_len, List<String> prompt_history, List<String> agent_response_history) async 
{
    locked = true;

    List<String> prompt_history_copy = [...prompt_history];
    prompt_history_copy.last = prompt_history_copy.last + ". Maximalni delka odpovedi je 16 slov";
    // prompt_history[0] = prompt_history[0] + ". Maximalni delka odpovedi je 16 slov";
    var image_encoded = await encodeImageToBase64(imageFile);
    List<Map<String, dynamic>> messages = [];
    messages.add({
        "role": "user",
        "content": [
            {
                "type":"image",
                "source":{
                    "type": "base64",
                    "media_type":"image/png",
                    "data": image_encoded,
                },
            },{
              "type":"text",
              "text": prompt_history_copy[0]
            }
        ]
    });
    for(int i =  0; i < min(prompt_history.length-1, agent_response_history.length); i++){
        messages.add({
            "role": "assistant",
            "content": [{"type":"text", "text":agent_response_history[i]}]
        });
        messages.add({
            "role": "user",
            "content": [{"type":"text", "text":prompt_history_copy[i+1]}]
        });
    
    }
    print("SENDING MESSAGES !!!!!!!!!!!!!!!!!!!!!!!");

    HapticFeedback.lightImpact();
    EasyLoading.show(status: "Sending request to server..");

    print(messages); 
    var uri = Uri.parse('${env.server_url}/process_image');
    var headers = {
        'content-type': 'application/json',
        'Accept': 'application/json',
    };
    var body = {
        "messages": messages, 
        "language_in": lang_in,
        "language_out": lang_out,
        "max_length": max_len,
        "password": 'bajsdpf4545',
    };
    const max_retries =3;
    for(int i =0; i < max_retries; i++){
        try {
            var response= await http.post(uri, headers: headers, body: json.encode(body));
            EasyLoading.dismiss();

            // var response = await http.Response.fromStream(streamedResponse);
            if (response.statusCode == 200) {
                print("success ${response.body}");
                var data = await onSuccessfulImagePost(response);
                locked = false;
                return data;
            }else{
                print("error ${response.body}");    
            }
        } catch (e) {
            print("error ${e}");
            locked = false; 
            rethrow;
        }
    }

    locked = false;

    ttsService.tts_api("giving up on posting image");
    EasyLoading.showError("Request failed after 3 tries.");
    return "";

}

void onSuccessfulImagePost(http.Response response) {
    Map<String, dynamic> data = jsonDecode(response.body);
    print(data["response_message"]);
    TTSService().tts_api(data["response_message"]);
    return Future.value(data["response_message"]);
}

Future<String> encodeImageToBase64(File imageFile) async {
  List<int> imageBytes = await imageFile.readAsBytesSync();
  String base64Image = base64Encode(imageBytes);
  return base64Image;
}
