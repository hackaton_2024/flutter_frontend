import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:hackaton/app.dart';
import 'package:camera/camera.dart';
import 'package:hackaton/services/tts-service.dart';
import 'package:hackaton/services/server-requests.dart';
import 'package:hackaton/services/asr-service.dart';
import 'package:hackaton/services/tts-service.dart';
import 'package:path_provider/path_provider.dart';
//import 'package:hackaton/services/server-requests.dart';

void main() async {
    WidgetsFlutterBinding.ensureInitialized();

    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp, DeviceOrientation.portraitDown
    ]);

    TTSService ttsService = TTSService();

    final camera = await load_cameras();

    // customize easyloading
    EasyLoading.instance
      ..displayDuration = const Duration(milliseconds: 2000)
      ..indicatorType = EasyLoadingIndicatorType.ring
      ..loadingStyle = EasyLoadingStyle.dark
      ..indicatorSize = 50.0
      ..textStyle = const TextStyle(fontSize: 16.0)
      ..radius = 15.0
      ..progressColor = Colors.yellow
      ..indicatorColor = Colors.yellow
      ..textColor = Colors.yellow
      ..maskColor = Colors.blue.withOpacity(0.5)
      ..userInteractions = true
      ..dismissOnTap = false;

    // runApp(builder: (_)=>App(camera: camera));
    runApp(App(camera:camera));
}

Future<CameraDescription> load_cameras() async {
  WidgetsFlutterBinding.ensureInitialized();
  final cameras = await availableCameras();
  for (var camera in cameras) {
    if (camera.lensDirection == CameraLensDirection.back) {
      print("camera has been found");
      return camera;
    }
  }
  throw Exception("Back camera not found");
}
