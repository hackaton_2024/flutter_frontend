import 'package:flutter/material.dart';
// const String server_url = 'http://192.168.32.56:5000';
const String server_url = 'https://vc3lv54ss9.execute-api.us-west-2.amazonaws.com/DEV';
const String config_max_len_key = 'max_len';
const Color config_background_color= Colors.black54;
const Color config_foreground_color = Colors.white;
const Color config_secondary_background_color =  Color(0xFF212121);
const Color config_accent_color = Colors.purpleAccent;
