import 'package:path_provider/path_provider.dart';
import 'dart:convert';
import 'dart:io';

// class JsonHelper(){


  Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();
  return directory.path;
  }

  Future<File> get promptsFile async {
    final path = await _localPath;
    return File('$path/prompts.json');
  }
  Future<Map<String, dynamic>> get jsonSettings async {
     File f = await promptsFile;
     return jsonDecode(await f.readAsString());
  }

// }
