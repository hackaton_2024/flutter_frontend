import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:hackaton/ui/pages/voice_record.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:hackaton/services/server-requests.dart';
import 'package:hackaton/langs.dart';
import 'package:hackaton/services/tts-service.dart';

import 'package:hackaton/core/jsonHelper.dart';


enum ZoomDirection {
  inZoom,
  outZoom,
}

double zoomLevel = 1.0;
FocusMode focusMode = FocusMode.auto;

// A screen that allows users to take a picture using a given camera.
class CameraPictureScreen extends StatefulWidget {
  CameraPictureScreen({
    super.key,
    required this.camera,
  });

  final CameraDescription camera;

  @override
  CameraPictureScreenState createState() => CameraPictureScreenState();
}

class CameraPictureScreenState extends State<CameraPictureScreen> {
  late CameraController _controller;
  Future<void>? _initializeControllerFuture;
  final TTSService ttsService = TTSService();
  int _retryCount = 0;
  final int _maxRetryCount = 5;

  @override
  void initState() {
      initAsync();
      super.initState();
  }
  Future<void> initAsync() async {
      var json = await jsonSettings;
      var res = json['resolution'];
      if (res != null) {
        initializeCamera(res);
      }else{
        initializeCamera('Medium');
      }

  }
    Future<void> restart_camera(resolution) async {
        super.dispose();
      // if (_controller != null) {
          _controller.dispose();
      // }
      // _controller = null;
      // _initializeControllerFuture = Future(null)
      initializeCamera(resolution);
    }
  Future<void> initializeCamera(String resolution) async {
    ResolutionPreset preset ;
    switch (resolution) {
      case "Low":
        preset = ResolutionPreset.low;
        break;
      case "Medium":
        preset = ResolutionPreset.medium;
        break;
      case "High":
        preset = ResolutionPreset.high;
        break;
      case "Ultra":
        preset = ResolutionPreset.ultraHigh;
        break;
      default:
        preset = ResolutionPreset.medium;
    }
    _controller = CameraController(
      widget.camera,

      preset,

    );
    _initializeControllerFuture = _controller.initialize().then((_) {
      //if (!mounted) return;
      setState(() {});
      _controller.setFlashMode(FlashMode.off);
      _retryCount = 0;
    }).catchError((Object e) async {
      if (e is CameraException) {
        _retryCount++;
        if (_retryCount <= _maxRetryCount) {
          EasyLoading.showError("Failed to initialize camera. Retrying... (attempt $_retryCount)",);
          print("Initialization failed, retrying... Attempt: $_retryCount");
          await Future.delayed(Duration(seconds: 1));
          var json = await jsonSettings;
          restart_camera(json['resolution']);
        } else {
          EasyLoading.showError("Failed to initialize camera after $_maxRetryCount attempts.",);
          print("Failed to initialize after $_maxRetryCount attempts.");
          ttsService.tts_api(
              "Camera could not be initialized after $_maxRetryCount attempts.");
        }
      } else {
        EasyLoading.showError("Failed to initialize camera. An unexpected error occurred",);
        print("An unexpected error occurred during camera initialization: $e");
      }
    });
  }

  Future<bool> check_camera_gestures() async {
    var localJsonSettings = await jsonSettings;

    return localJsonSettings['cameraGestures'] ?? true; 
  }

  void on_focus() async {
    if (!await check_camera_gestures()) return;

    HapticFeedback.vibrate();
    if (focusMode == FocusMode.auto) {
      focusMode = FocusMode.locked;
      EasyLoading.showToast("Auto focus off");
    } else if (focusMode == FocusMode.locked) {
      focusMode = FocusMode.auto;
      EasyLoading.showToast("Auto focus on");
    }

    await _controller.setFocusMode(focusMode);
    setState(() {});
  }

  void on_flashlight(bool? turnOff) async {
    if (!await check_camera_gestures()) return;

    HapticFeedback.vibrate();
    if (_controller.value.flashMode == FlashMode.always || turnOff == true) {
      await _controller.setFlashMode(FlashMode.off);
      if (turnOff == false) {
        EasyLoading.showToast("Turned off flash");
      }
    } else {
      await _controller.setFlashMode(FlashMode.always);
      EasyLoading.showToast("Turned on flash");
    }
    setState(() {});
  }

  void on_zoom(double zoomDelta) async {
    if (!await check_camera_gestures()) return;

    if (zoomDelta == 0) return;
    if (zoomLevel + -(zoomDelta * 0.1) < -1) return;
    if (zoomLevel + -(zoomDelta * 0.1) > 8) return;

    zoomLevel += -(zoomDelta * 0.1);

    await _controller.setZoomLevel(zoomLevel);

    EasyLoading.showToast("Zoomed ${zoomLevel.toStringAsFixed(1)}x", duration: const Duration(seconds: 2), toastPosition: EasyLoadingToastPosition.bottom);

    setState(() {});
  }

  void on_take_photo() async {
    HapticFeedback.vibrate();
    await _initializeControllerFuture;
    if (!mounted) return;
    await _controller.setFocusMode(FocusMode.locked);
    EasyLoading.show(status: "Saving photo...");
    setState(() {});
    final image = await _controller.takePicture();
    await _controller.setFocusMode(FocusMode.auto);
    on_flashlight(true);
    EasyLoading.dismiss();

    await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => VoiceRecordPage(imagePath: image.path
            // Pass the automatically generated path to
            // the DisplayPictureScreen widget.
            ),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<void>(
      future: _initializeControllerFuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          final double screenHeight = MediaQuery.of(context).size.height;
          final double appBarHeight = Scaffold.of(context).appBarMaxHeight ?? 0;
          final double remainingHeight =
              screenHeight - appBarHeight - (screenHeight * 2 / 3);

          return Column(
            children: <Widget>[
              Container(
                height: screenHeight * 2 / 3 * 1.1,
                child: CameraPreview(_controller),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
            ],
          );
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}

class DisplayPictureScreen extends StatelessWidget {
  const DisplayPictureScreen({super.key, required this.imagePath});

  final String imagePath;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Display the Picture')),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Image.file(File(imagePath)),
    );
  }
}
