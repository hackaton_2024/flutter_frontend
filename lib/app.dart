import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:hackaton/ui/pages/home-page.dart';
import 'package:hackaton/ui/components/top-bar.dart';
import 'package:camera/camera.dart';
import 'package:hackaton/app_config.dart' as env;
import 'package:hackaton/ui/pages/settings-page.dart';
import 'package:hackaton/ui/pages/voice_record.dart';
import 'package:hackaton/ui/pages/results.dart';

class App extends StatelessWidget {
  App({super.key, required this.camera});
  final CameraDescription camera;
  
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        // initialRoute: "/",
        providers: [ChangeNotifierProvider(create: (_) => AppState())],    
        child: ChangeNotifierProvider(
          create: (context) => AppState(),
          child: MaterialApp(
            title: 'Flutter Demo dz',
            theme: ThemeData(
              useMaterial3: true,
              // colorScheme: color_theme,
              scaffoldBackgroundColor: Colors.black54,
              brightness: Brightness.dark,
            ),
            initialRoute: "/",
            routes: {
                '/settings': (context) => SettingsPage(),
            },
            builder: EasyLoading.init(),

            debugShowCheckedModeBanner: false,
            home: Scaffold(
                appBar: PreferredSize(
                    preferredSize: const Size.fromHeight(55.0),
                    child:  Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TopBar(),        
                        ),
                ),
                body: HomePage(camera: camera),
            ),
          ),
        ),
    );
    // var color_theme = ColorScheme.fromSwatch(
    //   brightness: Brightness.dark,
    //   primarySwatch: env.config_accent_color,
    // );
  }
}

class AppState extends ChangeNotifier {
}
